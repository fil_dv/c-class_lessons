﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_2_list_generic
{
    public class Node<T> where T : IComparable<T>
    {
        T _value;
        public T Value { get { return _value; } set { _value = value; } }
        Node<T> _next;
        public Node<T> Next { get { return _next; } set { _next = value; } }
        Node<T> _prew;
        public Node<T> Prew { get { return _prew; } set { _prew = value; } }
        
        public Node(T value)
        {
            _value = value;
            _next = null;
            _prew = null;
        }

        public override string ToString()
        {  
            return string.Format("previous node: {0}      \t value this node: {1}  \t next node: {2}\n", _prew == null ? string.Format("have not") : _prew.Value.ToString(), Value, _next == null? string.Format("have not") : _next.Value.ToString());            
        }
    }
}
