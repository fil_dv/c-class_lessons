﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_2_list_generic
{
    class DList<T> : ICollection<T>, IEnumerable<T> where T : IComparable<T>
    {
        Node<T> _head;
        Node<T> _back;

        public DList()
        {
            _head = null;
            _back = null;
        }
        
        public void PrintList()
        {
            if (_head != null)
            {
                Node<T> cur = _head;
                do
                {
                    Console.WriteLine(cur);
                    cur = cur.Next;
                }
                while (cur != null);
                Console.WriteLine();
            }
            else throw new Exception("list is empty");
        }       
        
        public void PushBack(T item)
        {
            Node<T> newNode = new Node<T>(item);
            if (_head == null)
            {
                _head = newNode;
                _back = newNode;
            }
            else
            { 
                if (_head == _back)
                {
                    _head.Next = _back.Prew; 
                } 
                newNode.Prew = _back;
                _back.Next = newNode;
                _back = newNode;                
            }
        }

        public void PushFront(T item)
        {
            Node<T> newNode = new Node<T>(item);
            if (_head == null)
            {
                _head = newNode;
                _back = newNode;
            }
            else
            {
                if (_head == _back)
                {
                    _head.Next = _back.Prew;
                }
                newNode.Next = _head;
                _head.Prew = newNode;
                _head = newNode;
            }
        }

        public void PopBack()
        {
            if (_head == null) throw new Exception("List is empty");
            if (_head == _back)
            {
                _head = null;
                _back = null;
                return;
            }
            _back = _back.Prew;  // сдвигаем список на узел назад
            _back.Next = null;   // отцепляем "последний" элемент
        }

        public void PopFront()
        {
            if (_head == null) throw new Exception("List is empty");
            if (_head == _back)
            {
                _head = null;
                _back = null;
                return;
            }
            _head = _head.Next;  // сдвигаем список на узел вперед
            _head.Prew = null;   // отцепляем "первый" элемент
        }

        public T Front 
        { 
            get
            {
               if (_head == null) throw new Exception("list is empty");
               return _head.Value; 
            } 
        }
        public T Back 
        { 
            get 
            {
                if (_head == null) throw new Exception("list is empty");
                return _back.Value; 
            } 
        }

        public IEnumerator<T> GetEnumerator()
        {
            return (IEnumerator<T>)this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {            
            PushBack(item);
        }

        public void Clear()
        {
            _head = null;
            _back = null;
        }

        public bool Contains(T item)
        {
            if (_head != null)
            {
                Node<T> cur = _head;
                do
                {
                    if (cur.Value.CompareTo(item) == 0)
                    {
                        return true;
                    }

                    cur = cur.Next;
                }
                while (cur != null);
                return false;
            }
            else throw new Exception("list is empty");          
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (arrayIndex <= 0) throw new ArgumentException();            
            int i = 0;
            if (_head != null)
            {
                Node<T> cur = _head;
                do
                {
                    array[i] = cur.Value;
                    i++;
                    cur = cur.Next;
                }
                while (cur != null || arrayIndex > this.Count);
            }
            else throw new Exception("list is empty"); 
        }

        public int Count
        {
            get
            {
                if (_head != null)
                {
                    int i = 0;
                    Node<T> cur = _head;
                    do
                    {
                        i++;
                        cur = cur.Next;
                    }
                    while (cur != null);
                    return i;
                }
                else throw new Exception("list is empty");    
            }            
        }

        public bool IsReadOnly
        {
            get 
            {
                return false;
            }
        }

        public bool Remove(T item)
        {            
            if (_head != null)
            {
                Node<T> cur = _head;
                do
                {
                    if (cur.Value.CompareTo(item) == 0)
                    {
                        if (_head == _back)
                        {
                            _head = null;
                            _back = null;                           
                        }
                        else
                        {
                            cur.Prew.Next = cur.Next;
                            cur.Next.Prew = cur.Prew;
                            cur = null;
                        }
                        return true;
                    }
                    cur = cur.Next;
                }
                while (cur != null);
                return false;  
            }
            else throw new Exception("list is empty"); 
        }
    }
}
