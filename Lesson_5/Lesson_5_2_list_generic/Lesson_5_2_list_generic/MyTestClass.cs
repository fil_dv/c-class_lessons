﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_2_list_generic
{
    class MyTestClass : IComparable<MyTestClass>
    {
        int _value;
        public int Value{get{return _value;} set{_value = value;}}

        public MyTestClass(int i)
        {
            _value = i;
        }

        public int CompareTo(MyTestClass other)
        {
            if (_value < other.Value) return -1;
            else if (_value == other.Value) return 0;
            else return 1;
        }

        public override string ToString()
        {
            return string.Format(_value.ToString());
        }
    }
}
