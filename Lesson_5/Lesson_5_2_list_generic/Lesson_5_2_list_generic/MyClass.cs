﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_2_list_generic
{
    class MyClass : MyCopareInterface<MyClass>
    {
        int _value;
        public int Value{get{return _value;} set{_value = value;}}

        public MyClass(int i)
        {
            _value = i;
        }

        public bool IsEquel(MyClass m)
        {
            return _value == m._value;
        }
    }
}
