﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_2_list_generic
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MyTestClass my1 = new MyTestClass(111);
                MyTestClass my2 = new MyTestClass(222);
                MyTestClass my3 = new MyTestClass(333);
                MyTestClass my4 = new MyTestClass(444);

                DList<MyTestClass> list = new DList<MyTestClass>();
               
                list.PushBack(my1);         
                list.PushBack(my2);
                list.PushBack(my3);           

                //list.PushFront(my1);
                //list.PushFront(my2);
                // list.PushFront(my3);

                //list.PopBack();
                //list.PopFront();                
               
                //if (list.Contains(my3)) Console.WriteLine("element is present");
                //else Console.WriteLine("element is not present");

                //MyClass[] myArr = new MyClass[list.Count];
                //list.CopyTo(myArr, list.Count);
                //for (int i = 0; i < myArr.Length; ++i)
                //{
                //    Console.WriteLine(myArr[i]);
                //}
                               
                list.PrintList();
                if (list.Remove(my2))
                {
                    list.PrintList();
                }
                else Console.WriteLine("element for remove is not present in list");                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
