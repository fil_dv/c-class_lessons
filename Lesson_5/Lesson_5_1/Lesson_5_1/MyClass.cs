﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_1
{
    class MyClass : IisZero, IAriphmethics<MyClass> 
    {
        int _value;
        public int Value { get { return _value; } }

        public MyClass(int v)
        {
            _value = v;
        }
        public bool isZero
        {
            get { return Value == 0; }
        }

        public bool isBigger
        {
            get { return Value > 0; }
        }

        public MyClass Multi(MyClass x)
        {
            return new MyClass(Value * x.Value);            
        }

        public MyClass Multi(int x)
        {
            return new MyClass(Value * x);
        }

        public MyClass Substr(MyClass x)
        {
            return new MyClass(Value - x.Value);
        }

        public MyClass SubstrUnar()
        {
            return new MyClass(-Value);
        }

        public MyClass Add(MyClass x)
        {
            return new MyClass(Value + x.Value);
        }

        public MyClass Div(MyClass x)
        {
            return new MyClass(Value / x.Value);
        }

        public MyClass Sqrt()
        {
            return new MyClass((int)Math.Sqrt(Value));
        }
    }
}
