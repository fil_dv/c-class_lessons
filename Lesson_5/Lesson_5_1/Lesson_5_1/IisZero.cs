﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_1
{
    interface IisZero
    {
        bool isZero { get; }
        bool isBigger { get; }
    }
}
