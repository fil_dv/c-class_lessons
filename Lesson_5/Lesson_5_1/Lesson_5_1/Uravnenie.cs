﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_1
{
    class Uravnenie<T> where T : IisZero, IAriphmethics<T> 
    {
        T _a;
        T _b;
        T _c;

        public Uravnenie(T a, T b, T c)
        {
            //_a = default(T);
            _a = a;
            _b = b;
            _c = c;
        }

        public void Calc(out T x1, out T x2)
        {
            if (_a.isZero) throw new Exception("first argument is zero, it is linear equation\n");
            T d = (_b.Multi(_b)).Substr(_a.Multi(_c).Multi(4));
            if (d.isBigger)
            {
                x1 = (_b.SubstrUnar().Add(d.Sqrt())).Div(_a.Multi(2));
                x2 = (_b.SubstrUnar().Substr(d.Sqrt())).Div(_a.Multi(2));
                return;
            }
            else if (d.isZero)
            {
                x1 = (_b.SubstrUnar().Add(d.Sqrt())).Div(_a.Multi(2));
                x2 = x1;
                return;
            }
            else throw new Exception("equation has no roots\n");
        }
    }
}
