﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_5_1
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass a = new MyClass(2);
            MyClass b = new MyClass(4);
            MyClass c = new MyClass(2);
            MyClass x1;
            MyClass x2;
            Uravnenie<MyClass> ur = new Uravnenie<MyClass>(a, b, c);
            try
            {
                ur.Calc(out x1, out x2);
                Console.WriteLine("x1 = {0} \nx2 = {1} \n", x1.Value, x2.Value);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
