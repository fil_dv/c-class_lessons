﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Uravnenie
    {
        public double _a;
        public double _b;
        public double _c;

        public Uravnenie(double a = 0, double b = 0, double c = 0)
        {
            this._a = a;
            this._b = b;
            this._c = c;
        }

        public bool Calculate(out double x1, out double x2)
        {
            bool b = false;
            double dis = _b * _b - 4 * _a * _c;
            if (dis < 0)
            {
                x1 = 0;
                x2 = 0;
                return b;
            }
            else
            {
                x1 = (-_b + Math.Sqrt(dis)) / 2 * _a;
                x2 = (-_b - Math.Sqrt(dis)) / 2 * _a;
                b = true;
                return b;
            }
        }
    }
}
