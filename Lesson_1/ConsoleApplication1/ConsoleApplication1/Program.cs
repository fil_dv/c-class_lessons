﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("Hello World! I am c#!");
            Console.WriteLine("What is your name?");
            string name = Console.ReadLine();
            Console.WriteLine("Hi, {0}", name);
            
            int a;
            Console.WriteLine("Enter integer number!");
            var tmp = Console.ReadLine();
            a = int.Parse(tmp);
            if (!int.TryParse(tmp, out a)) 
            {
                Console.WriteLine("Error!");
            }
            a = Convert.ToInt32(tmp);
             
            
            double vvod;
            Console.WriteLine("Enter radius!");
            var tmp = Console.ReadLine();
             if (!double.TryParse(tmp, out vvod)) 
            {
                Console.WriteLine("Error!");
                return;
            }

            double Ln = 2 * Math.PI * vvod;
            Console.WriteLine("long = {0}", Ln); 
            double Ar = Math.PI * vvod * vvod;
            Console.WriteLine("Area = {0}", Ar); 
           

            Human hum = new Human("Ivan", "Ivanov");
            hum.SetAge(23);
            hum.PrintHuman();
            hum.Name = "Petr";
            string name = hum.Name;
            hum.PrintHuman();
            */

            double x1, x2;
            double a, b, c;
            Console.WriteLine("Enter first number.");
            var tmp = Console.ReadLine();
            if (!double.TryParse(tmp, out a))
            {
                Console.WriteLine("Error!");
                return;
            }
            Console.WriteLine("Enter second number.");
            tmp = Console.ReadLine();
            if (!double.TryParse(tmp, out b))
            {
                Console.WriteLine("Error!");
                return;
            }
            Console.WriteLine("Enter third number.");
            tmp = Console.ReadLine();
            if (!double.TryParse(tmp, out c))
            {
                Console.WriteLine("Error!");
                return;
            }
            
            Uravnenie ur = new Uravnenie(a, b, c);
            if (!ur.Calculate(out x1, out x2))
            {
                Console.WriteLine("The equation has no solutions!");
            }
            else
            {
                Console.WriteLine("x1 = {0}, x2 = {0} ", x1, x2);                
            }
        }





        public class Human
        {
            public Human()
            { 
                
            }
            public Human(string name, string surname)
            {
                _name = name;
                _surname = surname;
                _age = 0;
            }
            ~Human()
            {
                
            }

            private string _name;
            private string _surname;
            int _age;
            public const int a = 123;
            public readonly double MinAge = 18;
            public static string B;


            public string Name
            {
                get
                {
                    return this._name;
                }
                set
                {
                    this._name = value;
                }
            }

            public int GetAge()
            {
                return this._age;
            }
            
            public void SetAge(int age)
            {
                this._age = age;
            }

            public void SetNameSurname(string name, string surname)
            {
                this._name = name;
                this._surname = surname;
            }

            public void SetNameSurname(string name)
            {
                SetNameSurname(name, string.Empty);
            }

            public void SetNameSurname(int age)
            {
                this._age = age;
            }

            public DateTime BirthDay
            {
                get;
                private set;
            }

            public void PrintHuman()
            {
                Console.WriteLine("name - {0}, surname - {1}, age - {2}", _name, _surname, _age); 
            }

            public static Human CreateHuman(string name, string surname)
            {
                Human _hum = new Human(name, surname);
                return _hum;
            }
        }




       



    }
}
