﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_4_1
{   
    class InvalidYearException : MyException
    {
        public InvalidYearException()
            : base("Invalid value of year") { }

        public InvalidYearException(string message)
            : base(message) { }

        public InvalidYearException(string message, Exception innerException)
            : base(message, innerException) { }

        public InvalidYearException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }    
}
