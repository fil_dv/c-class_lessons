﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_4_1
{
    class InvalidDayException : MyException
    {
        public InvalidDayException()
            : base() { }

        public InvalidDayException(string message)
            : base(message) {}

        public InvalidDayException(string message, Exception innerException)
            : base(message, innerException) { }

        public InvalidDayException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
