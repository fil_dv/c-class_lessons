﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_4_1
{
    class InvalidMonthException: MyException
    {
        public InvalidMonthException()
            : base() { }

        public InvalidMonthException(string message)
            : base(message) {  }

        public InvalidMonthException(string message, Exception innerException)
            : base(message, innerException) { }

        public InvalidMonthException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
