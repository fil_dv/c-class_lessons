﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_4_1
{
     struct Date
        {
            int _d, _m, _y;
            public Date(int y, int m, int d)
            {
                _d = d;
                _m = m;
                _y = y;
                isOk();             
            }

            bool isOk()
            {
                if (_y < 0 || _y > 3000) { throw new InvalidYearException("Invalid value of year"); }
                else
                    if (_m < 1 || _m > 12) { throw new InvalidMonthException("Invalid value of month"); }
                    else
                    {
                        switch (_m)
                        {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if (_d < 1 || _d > 31) { throw new InvalidDayException("Invalid value of day"); }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if (_d < 1 || _d > 30) { throw new InvalidDayException("Invalid value of day"); }
                                break;
                            case 2:
                                if (_d < 1 || _d > 29) { throw new InvalidDayException("Invalid value of day"); }
                                else
                                    if (_d == 29 && _y % 4 != 0) { throw new InvalidDayException("Invalid value of day"); }
                                break;
                        }
                        return true;
                    }
            }
            
            public int Day { get { return _d; } }
            public int Month { get { return _m; } }
            public int Year { get { return _y; } }

            public override string ToString()
            {
                DateTime d = new DateTime(_y, _m, _d);
                return string.Format("{0:dd.MM.yyyy}", d); 
            }

            static public Date operator +(Date d1, int days)
            {
                DateTime Day = new DateTime(d1._y, d1._m, d1._d);                
                DateTime DayRes = Day.AddDays(days);
                Date Res = new Date();
                Res._y = DayRes.Year;
                Res._m = DayRes.Month;
                Res._d = DayRes.Day;
                return Res;
            }

            static public bool operator >(Date d1, Date d2)
            {
                DateTime Day1 = new DateTime(d1._y, d1._m, d1._d);
                DateTime Day2 = new DateTime(d2._y, d2._m, d2._d);
                return (Day1 > Day2);
            }

            static public bool operator <(Date d1, Date d2)
            {
                DateTime Day1 = new DateTime(d1._y, d1._m, d1._d);
                DateTime Day2 = new DateTime(d2._y, d2._m, d2._d);
                return Day1 < Day2;
            }

            static public bool operator ==(Date d1, Date d2)
            {
                DateTime Day1 = new DateTime(d1._y, d1._m, d1._d);
                DateTime Day2 = new DateTime(d2._y, d2._m, d2._d);
                return Day1 == Day2;
            }

            static public bool operator !=(Date d1, Date d2)
            {
                DateTime Day1 = new DateTime(d1._y, d1._m, d1._d);
                DateTime Day2 = new DateTime(d2._y, d2._m, d2._d);
                return Day1 != Day2;
            }
            

            


        }

}
