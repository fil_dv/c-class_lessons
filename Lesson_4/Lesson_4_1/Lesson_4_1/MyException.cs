﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_4_1
{   
    class MyException : ApplicationException
    {
        public MyException()
            : base() { }

        public MyException(string message)
            : base(message) { }

        public MyException(string message, Exception innerException)
            : base(message, innerException) { }

        public MyException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }    
}
