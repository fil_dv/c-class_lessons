﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_4_1
{
    class Program
    {      
        static void Main(string[] args)
        {
            try
            {
                Date d1 = new Date(2015, 3, 31);
                Date d2 = new Date(2015, 2, 18);
                Console.WriteLine(d1);
                Date res = d1 + 5;
                Console.WriteLine(res);
                if (d1 > d2) Console.WriteLine("{0} > {1}",d1,d2);
                else Console.WriteLine("{0} < {1}", d1, d2);

                if (d1 == d2) Console.WriteLine("{0} == {1} it`s tru!", d1, d2);
                else Console.WriteLine("{0} == {1} it`s false!", d1, d2);
            }
            catch (MyException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
