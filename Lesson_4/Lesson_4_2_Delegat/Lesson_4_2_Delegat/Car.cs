﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_4_2_Delegat
{
    delegate void SpeedChangeHandler(Car a);

    class Car
    {
        public int Speed { get; private set; }
        int count = 0;

        public void Acseleration()
        {
            Speed++;
            ReiseSpeedChange();
        }

        public void Break()
        {
            Speed--;
            ReiseSpeedChange();
        }

        protected void ReiseSpeedChange()
        {
            var Handler = SpeedChanged;
            if (Handler != null)
            {
                Handler(this);
            }
        }

        public event SpeedChangeHandler SpeedChanged;
    }
}
