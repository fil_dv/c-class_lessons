﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_4_2_Delegat
{
    delegate int OperationHandler(int a, int b);
    
    class Program
    {
        static int S1(int a, int b)
        {
            return a + b;
        }

        int S2(int a, int b)
        {
            return a * b;
        }
        
        static void Main(string[] args)
        {
          
            /*
            OperationHandler h;// = new OperationHandler(S1);
            h = S1;
            Console.WriteLine(h(2,3));
            h += (new Program()).S2;
            int c = h(3, 3);
            Console.WriteLine(c);
            //Console.WriteLine(h(3,3));
             */

            Car a = new Car();
            Car b = new Car();

            a.SpeedChanged += a_SpeedChanged;
            a.Acseleration();
            a.Acseleration();
            a.Acseleration();
            a.Break();
            int count = 0;
            b.SpeedChanged += d => { count++; };
            b.Acseleration();
            b.Acseleration();
            b.Acseleration();




        }

        static void a_SpeedChanged(Car a)
        {
            Console.WriteLine("current speed {0}", a.Speed);
        }
    }
}
