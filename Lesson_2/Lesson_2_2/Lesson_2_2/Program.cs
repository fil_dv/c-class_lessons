﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_2_2
{
    class Program
    {

        enum ArialStatus:short { unknown = -3, Late = -1, OnTime = 0, Early = 1 };
        enum Sex { male, femail};
        
        static void Main(string[] args)
        {
            ArialStatus ars = ArialStatus.OnTime;
            short a = (short)ars;
            var str = Enum.GetNames(typeof(ArialStatus));
            foreach (var i in str)
            {
                Console.WriteLine(i);
            }


            short tmp;
            var str1 = Enum.GetValues(typeof(ArialStatus));
            foreach (var i in str1)
            {
                tmp = (short)i;
                Console.WriteLine(tmp);
            }

            Sex s;
            Console.WriteLine("enter your sex:");

            try
            {
                s = (Sex)Enum.Parse(typeof(Sex), Console.ReadLine(), true);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                s = Sex.male;
            }
            
            
            
            var temp = Console.ReadLine();
            if (!Sex.TryParse(temp, out s))
            {
                Console.WriteLine("Error!");
            }


            
            /*
            int a = 3;
            string s = string.Format("Квадрат {0,-33:E} равен {1,5:N}", a, a * a);
            Console.WriteLine(s);

            var d = DateTime.Now;
            Console.WriteLine(string.Format("{0:D}", d));
            Console.WriteLine(string.Format("{0:d}", d));
            Console.WriteLine(string.Format("{0:f}", d));

            Console.WriteLine(string.Format("{0:ddd dd.MM.yyyy h:mm:ss K}", d));

            int car = 123;
            Console.WriteLine(string.Format("{0, 5:000c}", car));
            Console.WriteLine(string.Format("{0, 5:{{c'}", car));
            */

            
            



        }
    }
}
