﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_2_1
{
    class myArray
    {
        private int _len;
        int[,] _arr;
        int[,] dest;

        public myArray()
        {
            _len = 5;
            _arr = new int[_len, _len];
            dest = new int[_len, _len];
        }

        private void CopyToDesk()
        {
            for (int i = 0; i < _len; ++i)
            {
                for (int j = 0; j < _len; ++j)
                {
                    dest[i, j] = _arr[i, j];
                }
            }          
        }

        private void CopyToThis()
        {
            for (int i = 0; i < _len; ++i)
            {
                for (int j = 0; j < _len; ++j)
                {
                    _arr[i, j] = dest[i, j];
                }
            }
        }



        public void ShiftVer(int index, int pos)
        {

            CopyToDesk();                        
            
            for (int i = 0; i < _len; ++i)
            {
                for (int j = 0; j < _len; ++j)
                {
                    if (j == index)
                    {
                        if (pos - i > 0)
                        {
                            dest[i, index] = _arr[_len + i - pos, j];
                        }
                        else
                        {
                            dest[i, index] = _arr[i - pos, j];
                        }
                    }
                }
            }

            CopyToThis();
        }

        public void ShiftHor(int index, int pos)
        {
            CopyToDesk();  
           
             for (int i = 0; i < _len; ++i)
             {
                 for (int j = 0; j < _len; ++j)
                 {
                     if (i == index)
                     {
                         if (pos - j > 0)
                         {
                             dest[index, j] = _arr[i, _len + j - pos];
                         }
                         else
                         {
                             dest[index, j] = _arr[i, j - pos];
                         }
                     }
                 }
             }

             CopyToThis();
        }

        
        public void Print()
        {
            for (int i = 0; i < _len; ++i)
            {
                for (int j = 0; j < _len; ++j)
                {
                    Console.Write(_arr[i,j]);
                }
                Console.WriteLine();
            }

        }
        
        public void init()
        {
            for (int i = 0; i < _len; ++i)
            {
                for (int j = 0; j < _len; ++j)
                {
                    _arr[i, j] = i+1;
                }
            }
        }

        public void initRand()
        {
            for (int i = 0; i < _len; ++i)
            {
                for (int j = 0; j < _len; ++j)
                {
                   // _arr[i, j] = Random.Next(10); ???????????????????????
                }
            }
        }
    }
}
