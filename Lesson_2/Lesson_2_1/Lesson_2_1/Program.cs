﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_2_1
{
    public class Program
    {
        static void Main(string[] args)
        {           
         myArray myArr = new myArray();
         myArr.init();            
         char isExit = '1';
         int vvod;
         int line;
         int pos;
         myArr.Print();
         while (isExit != '0')
         {            
             Console.WriteLine("select number of line to shift: ");
             var tmp = Console.ReadLine();
             if (!int.TryParse(tmp, out line) || line < 1 && line > 5)
             {
                 Console.WriteLine("Error!");
             }
             Console.WriteLine("select number simbols to shift: ");
             tmp = Console.ReadLine();
             if (!int.TryParse(tmp, out pos) || line < 1 && line > 5)
             {
                 Console.WriteLine("Error!");
             }
             Console.WriteLine("press \"1\" for horisintal shift or \"2\" for vertical shift ");
             tmp = Console.ReadLine();
             if (!int.TryParse(tmp, out vvod) || vvod < 1 && vvod > 2)
             {
                 Console.WriteLine("Error!");
             }
             if (vvod == 1)
             {
                 myArr.ShiftHor(line-1, pos);                   
             }
             else
             {
                 myArr.ShiftVer(line-1, pos);                   
             }
             myArr.Print();
             Console.WriteLine();
             Console.WriteLine("press \"0\" for exit"); 
             Console.WriteLine("or any another key for continue:");
             tmp = Console.ReadLine();
             if (!char.TryParse(tmp, out isExit))
             {
                 Console.WriteLine("Error!");
             }
          }
       }       
    }
}
