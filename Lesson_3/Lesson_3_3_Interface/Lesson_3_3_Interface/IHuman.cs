﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_3_Interface
{
    interface IHuman
    {
        string Name { get; set; }
        void Work();
    }
}
