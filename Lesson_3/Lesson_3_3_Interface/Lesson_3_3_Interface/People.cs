﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Lesson_3_1;

namespace Lesson_3_3_Interface
{
    class People : IEnumerable
    {
        private Human[] _people;
        public People(Human[] p)
        {
            _people = new Human[p.Length];
            p.CopyTo(_people, 0);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public PeopleEnum GetEnumerator()
        { 
            return new PeopleEnum(_people);
        }

    }
}
