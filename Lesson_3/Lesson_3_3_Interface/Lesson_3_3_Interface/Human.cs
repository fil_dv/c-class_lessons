﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_1
{
    class Human
    {
       // public string _name;
       // public string _surname;
        //public DateTime _birthday;

        public Human(string name, string surname/*, DateTime birthday*/)
        {
            Name = name;
            Surname = surname;
            //_birthday = birthday;
        }

        public string Name { get; set; }

        public string Surname { get; set; }

        /*int Birthday
        {
            get;
            set;
        }*/

        public virtual void Work()
        {
            Console.WriteLine("is working!");
        }

        public void Sleep()
        {
            Console.WriteLine("human is sleeping!");
        }



    }
}
