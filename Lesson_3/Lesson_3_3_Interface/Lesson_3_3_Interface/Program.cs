﻿using Lesson_3_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_3_Interface
{
    class Program
    {
        struct Student : IWorker, IHuman
        {
            string _name;

            void IHuman.Work()
            {
                Console.WriteLine("I`m working!");
            }

            string IHuman.Name { get; set; }

            void IWorker.Work()
            {
                throw new NotImplementedException();
            }
        }
        
        
        static void Main(string[] args)
        {
            Human[] h = new Human[3] { new Human("ivan", "ivanov"), new Human("petr", "petrov"), new Human("kolya", "kolin") };
            People p = new People(h);
            foreach (var i in p)
            {
                Console.WriteLine("{0} {1}", i.Name, i.Surname);
            }
          
        }
    }
}
