﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_2
{
    class Complex
    {
        int _a{get; set;}
        int _b;

        public Complex(int a = 0, int b = 0)
        {
            _a = a;
            _b = b;
        }

        static public Complex operator +(Complex x, Complex y)
        {
            return new Complex(x._a + y._a, x._b + y._b);            
        }

        static public Complex operator -(Complex x, Complex y)
        {
            return new Complex(x._a - y._a, x._b - y._b);
        }

        static public Complex operator *(Complex x, Complex y)
        {
            return new Complex((x._a * y._a - x._b * y._b), (x._b * y._a + x._a * y._b));
        }

        static public Complex operator *(Complex x, int y)
        {
            return new Complex(x._a * y, x._b * y);
        }

        static public Complex operator /(Complex x, Complex y)
        {
            return new Complex(((x._a * y._a + x._b * y._b) / (y._a * y._a + y._b * y._b)), ((x._b * y._a - x._a * x._b) / (x._b * x._b + y._b * y._b)));
        }

        static public bool operator ==(Complex x, Complex y)
        {
            return (x._a == y._a && x._b == y._b);
        }

        static public bool operator !=(Complex x, Complex y)
        {
            return (x._a != y._a || x._b != y._b);
        }

        public void Print()
        {
            Console.WriteLine("{0} + {1}i", _a, _b);
        }
    }
}
