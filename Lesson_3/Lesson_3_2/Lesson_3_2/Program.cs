﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Complex c = new Complex(2, 2);
            Complex d = new Complex(3, 3);
            Complex res = new Complex();
            c.Print();
            d.Print();
            res = c + d;
            res.Print();
            res = c - d;
            res.Print();
            res = c * d;
            res.Print();
            res = c / d;
            res.Print();
            res = c * 5;
            res.Print();
            if (c == d) Console.WriteLine("Complex numbers are equel");
            else Console.WriteLine("Complex numbers are not equel");
            if (c != d) Console.WriteLine("Complex numbers are not equel");
            else Console.WriteLine("Complex numbers are equel");
        }
    }
}
