﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_1
{
    class CPoint
    {
        int _x;
        int _y;

        public int x
        {
            get { return _x; }
            set { _x = value; }
        }

        public int y
        {
            get { return _y; }
            set { _y = value; }
        }

        static public CPoint operator++(CPoint p)
        {
            p._x++;
            p._y++;
            return p;
        }

        static public CPoint operator --(CPoint p)
        {
            p._x--;
            p._y--;
            return p;
        }

        static public CPoint operator -(CPoint p)
        {
            CPoint res = new CPoint();
            res._x = -p._x;
            res._y = -p._y;
            return res;
        }

        public static CPoint operator +(CPoint a, CPoint b)
        {
            return new CPoint() { x = a._x + b._x, y = a._y + b._y };
        }

        public static int operator *(CPoint a, CPoint b)
        {
            return a._x * b._x + a._y * b._y;               
        }

        static public CPoint operator *(CPoint a, int b)
        {
            return new CPoint() { x  = a._x * b, y = a._y * b };            
        }

        static public bool operator ==(CPoint a, CPoint b)
        {
            return (a._x == b._x && a._y == b._y);  
        }

        static public bool operator !=(CPoint a, CPoint b)
        {
            return (a._x != b._x || a._y != b._y);
        }

        static public bool operator true(CPoint a)
        {
            return (a._x > 0 && a._y > 0);
        }

        static public bool operator false(CPoint a)
        {
            return (a._x < 0 && a._y < 0);
        }

        static public explicit operator int(CPoint p)
        {
            return (int)Math.Sqrt(p._x * p._x + p._y * p._y);
        }

        static public implicit operator double(CPoint p)
        {
            return (int)Math.Sqrt(p._x * p._x + p._y * p._y);
        }

        public int this[int index]
        {
            get 
            {
                if (index == 0)
                    return _x;
                else if (index == 1)
                    return _y;
                else 
                    throw new ArgumentOutOfRangeException();
            }
            set
            {
                if (index == 0)
                    _x = value;
                else if (index == 1)
                    _y = value;
                else 
                    throw new ArgumentOutOfRangeException();
            }
        }







    }
}
