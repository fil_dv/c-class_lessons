﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_1
{
    class Student : Human
    {
        public Student(string name, string surname, DateTime birthday)
            : base(name, surname, birthday)
        { 
            
        }

        public override void Work()
        {
            Console.WriteLine("is studing!");
        }

        public new void Sleep()
        {
            Console.WriteLine("student is studing!");
        }
    }
}
