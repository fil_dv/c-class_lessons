﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_1
{
    class Program
    {
        static void Main(string[] args)
        {
           /* byte b = 100;
            byte c = 0;
            try
            {
                //c = checked(byte)(b + 200);
                checked
                {
                    c = (byte)(b + 200);
                }
            }
            catch (OverflowException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine(c);
            */

            CPoint p = new CPoint() { x = 2, y = 3 };
            p++;
            p--;

            Human h = new Human("Ivan", "Ivanov", DateTime.Now);
            Student s = new Student("Ivan", "Ivanov", DateTime.Now);
            h.Work();
            s.Work();
            h.Sleep();
            s.Sleep();


        }
    }
}
