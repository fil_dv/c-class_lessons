﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3_1
{
    class Human
    {
        public string _name;
        public string _surname;
        public DateTime _birthday;

        public Human(string name, string surname, DateTime birthday)
        {
            _name = name;
            _surname = surname;
            _birthday = birthday;
        }

        string Name
        {
            get;
            set;
        }

        string Surname
        {
            get;
            set;
        }

        int Birthday
        {
            get;
            set;
        }

        public virtual void Work()
        {
            Console.WriteLine("is working!");
        }

        public new void Sleep()
        {
            Console.WriteLine("human is sleeping!");
        }



    }
}
